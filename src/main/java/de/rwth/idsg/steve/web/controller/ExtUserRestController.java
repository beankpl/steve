package de.rwth.idsg.steve.web.controller;

import com.google.common.base.Strings;
import com.neovisionaries.i18n.CountryCode;
import de.rwth.idsg.steve.repository.OcppTagRepository;
import de.rwth.idsg.steve.repository.UserRepository;
import de.rwth.idsg.steve.repository.dto.User;
import de.rwth.idsg.steve.web.dto.Address;
import de.rwth.idsg.steve.web.dto.UserForm;
import de.rwth.idsg.steve.web.dto.UserQueryForm;
import de.rwth.idsg.steve.web.dto.UserSex;
import jooq.steve.db.tables.records.OcppTagRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.InvalidParameterException;
import java.util.List;

@RestController
@RequestMapping("/manager/ext/user")
public class ExtUserRestController {

    private UserRepository userRepository;
    private OcppTagRepository ocppTagRepository;

    @Autowired
    public ExtUserRestController(UserRepository userRepository, OcppTagRepository ocppTagRepository){
        this.userRepository = userRepository;
        this.ocppTagRepository = ocppTagRepository;
    }


    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User.Overview> createUser(@RequestBody User.Overview user ) throws InvalidParameterException {

        if(Strings.isNullOrEmpty(user.getEmail()))
            return ResponseEntity.badRequest().build();
        if(getUser(user.getEmail()) != null)
            return ResponseEntity.badRequest().build();
        if(user.getOcppIdTag() == null || user.getOcppIdTag().length() ==0)
            return ResponseEntity.badRequest().build();
        if(!isValidOcppTag(ocppTagRepository.getRecord(user.getOcppIdTag()) , user.getEmail()))
            return ResponseEntity.badRequest().build();

        UserForm newUser = new UserForm();
        newUser.setSex(UserSex.OTHER);
        newUser.setEMail(user.getEmail());
        newUser.setOcppIdTag(user.getOcppIdTag());
        Address address = new Address();
        address.setCity("MEL");
        address.setCountry(CountryCode.AU);
        address.setStreet("600 Bourke St");
        address.setZipCode("3000");
        newUser.setAddress( address);
        userRepository.add(newUser);
        return getUser(user.getEmail());
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User.Overview> getUser(@RequestParam("email")String email){
        if(Strings.isNullOrEmpty(email))
            return ResponseEntity.badRequest().build();
        UserQueryForm form = new UserQueryForm();
        form.setEmail(email);
        List<User.Overview> users = userRepository.getOverview(form);
        if(users.size() > 0) return ResponseEntity.ok(users.get(0));
        return ResponseEntity.ok(null);
    }


    private boolean isValidOcppTag(OcppTagRecord tag ,String email){
        if(tag != null && tag.getNote() != null && tag.getNote().equals(email)) return true;
        return false;
    }

}
