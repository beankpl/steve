package de.rwth.idsg.steve.web.controller;

import com.google.common.base.Strings;
import de.rwth.idsg.steve.repository.OcppTagRepository;
import de.rwth.idsg.steve.web.dto.OcppTagForm;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

@RestController
@RequestMapping("/manager/ext/tag")
public class ExtOcppTagRestController {
    private OcppTagRepository ocppTagRepository;

    @Autowired
    public ExtOcppTagRestController(OcppTagRepository ocppTagRepository){
        this.ocppTagRepository = ocppTagRepository;
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String,String>> createOcppTag(@RequestParam("email")String email){
        //TODO in order to avoid duplicated email association, need to change OcppTagQueryForm
        if(Strings.isNullOrEmpty(email))
            return ResponseEntity.badRequest().build();

        OcppTagForm tag = new OcppTagForm();
        tag.setIdTag(generateOcppTag());
        tag.setExpiration(LocalDateTime.now().plusYears(10));
        tag.setNote(email);
        ocppTagRepository.addOcppTag(tag);

        return ResponseEntity.ok(Collections.singletonMap("ocppIdTag", tag.getIdTag()));
    }

    //TODO need  ocpp IdTag generator for registering user
    private String generateOcppTag(){
        String uuid = UUID.randomUUID().toString().replace("-","");
        StringBuilder sb = new StringBuilder();
        Random rnd = new Random();
        for(int i =0 ; i < 20 ;i++){
            int n = rnd.nextInt(32);
            sb.append(uuid.charAt(n));
        }
        return sb.toString();
    }

}
