package de.rwth.idsg.steve.web.controller;

import de.rwth.idsg.steve.repository.ChargePointRepository;
import de.rwth.idsg.steve.repository.TransactionRepository;
import de.rwth.idsg.steve.repository.dto.Transaction;
import de.rwth.idsg.steve.repository.dto.TransactionDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/manager/ext")
public class ExtTransactionRestController {

    private TransactionRepository transactionRepository;
    private ChargePointRepository chargePointRepository;

    @Autowired
    public ExtTransactionRestController(TransactionRepository transactionRepository ,
                                        ChargePointRepository chargePointRepository){
        this.chargePointRepository = chargePointRepository;
        this.transactionRepository = transactionRepository;

    }

    @GetMapping(value = "/activeTx/{chargeBoxId}" , produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String,String> getActiveTransaction(@PathVariable("chargeBoxId") String chargeBoxId){
        //assume there should be only one active transaction if there're multiple , get latest one
        List<Integer> txIds = null;
        Transaction transaction = null;
        int count = 0;
        while(count < 3){ // 1 seconds at most
            txIds = transactionRepository.getActiveTransactionIds(chargeBoxId);
            if(txIds != null && !txIds.isEmpty()){
                TransactionDetails details = transactionRepository.getDetails(txIds.get(txIds.size() -1));//get latest one
                transaction  = details.getTransaction();
                break;
            }else{
                count++;
                sleep();
            }
        }
        Map<String,String> data = null;
        if(transaction != null){
            data = new HashMap<>();
            data.put("connectorId" , String.valueOf(transaction.getConnectorId()));
            data.put("chargeBoxId" , transaction.getChargeBoxId());
            data.put("ocppIdTag",transaction.getOcppIdTag());
            data.put("id" , String.valueOf(transaction.getId()));
            data.put("startTimestampDT", String.valueOf(transaction.getStartTimestampDT().getMillis()));
        }

        return data;
    }

    @GetMapping(value = "/stopTx/{transactionId}" ,produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String,String> getStopTransaciton(@PathVariable("transactionId") int transactionId){
        TransactionDetails details = transactionRepository.getDetails(transactionId);
        if(details == null) return null;
        Transaction txan = details.getTransaction();

        Map<String,String> data = null;
        if(txan.getStopTimestampDT() != null){
            data = new HashMap<>();
            data.put("connectorId" , String.valueOf(txan.getConnectorId()));
            data.put("chargeBoxId" , txan.getChargeBoxId());
            data.put("ocppIdTag",txan.getOcppIdTag());
            data.put("startValue" , txan.getStartValue());
            data.put("stopValue" , txan.getStopValue());
            data.put("startTimestamp", String.valueOf(txan.getStartTimestampDT().getMillis()));
            data.put("stopTimestamp" , String.valueOf(txan.getStopTimestampDT().getMillis()));
            data.put("stopReason" , txan.getStopReason());
        }

        return data;
    }

    private void sleep(){
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {

        }
    }
}
