package de.rwth.idsg.steve;

import java.util.UUID;

public class IdTagGenerator {

    public static String getRandomString() {
        return UUID.randomUUID().toString();
    }

    public static void main(String[] args) {
        System.out.println(getRandomString());
    }
}
